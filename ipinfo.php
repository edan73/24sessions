<?php

$ipinfo_token = NULL;
$aws_instance = NULL;
// need to set $ipinfo_token and $aws_instance here:
include_once "secrets.php";

require_once __DIR__ . '/vendor/autoload.php';
use Symfony\Component\HttpFoundation\Request;

$request = Request::createFromGlobals();

ActiveRecord\Config::initialize(function($cfg)
{
    global $aws_instance;
    $cfg->set_model_directory(__DIR__ . '/models');
    $cfg->set_connections(array('development' => "mysql://test_user:secret@$aws_instance/test"));
});

$app = new Silex\Application();

/*
 * The main handler to deliver the splash page.
 * Just the JQuery logic that calls the backend service
 */
$app->get('/', function () use ($app) {
    global $request;

    $result = '
<html><head><script src="jquery-3.3.1.js"></script>
<script>
    $(document).ready(function(){
      $("#getipinfo").click(function(e){
        $.ajax({type: "GET",
                url: "/ipinfo.php/getipinfo",
                success: function(result){
          $("#ipinforesult").html(result);
        }});
      });
    });
    </script>
</head><body>
';
    $result .= 'Your IP Address: ' . $request->getClientIp() . '<br/>';
    $result .= '<input type="button" name="getipinfo" id="getipinfo" value="Get geolocation"><br/>';
    $result .= '<div id="ipinforesult"></div>';
    $result .= '</body></html>';

    return $result;
});

/*
 * Helper function to retrieve the Gelocation info from the ipinfo.io service
 */
function retrieve_ipinfo( $addr ) {
    global $ipinfo_token;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://ipinfo.io/$addr?token=$ipinfo_token");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $response = curl_exec($ch);
    $json = json_decode($response, true);
    return $json;
}

/*
 * Backend handler to attempt to retrieve the IP address from the cache, 
 * or from the ipinfo.io service if there is no cache.
 */
$app->get('/getipinfo', function () use ($app) {
    global $request;

    $ip_address = $request->getClientIp();
    $city = NULL;
    $country = NULL;
    $result = "Your Gelocation info";

    try {
        $cached = Ipinfo::find('first', array('ip_address' => $ip_address));

        if ( $cached ) {
            $city = $cached->city;
            $country = $cached->country;
            $result .= " (cached)";
        } else {
            $json = retrieve_ipinfo( $ip_address );
            $city = $json['city'] ?: '<unknown>';
            $country = $json['country'] ?: '<unknown>';

            try {
                Ipinfo::create( array('ip_address' => $ip_address, 'city' => $city, 'country' => $country) );
            } catch ( Exception $e ) {
                // ignore
            }
        }
    } catch( Exception $e ) {
    }

    $result .= ": City: $city, Country: $country.  How's the weather there?";
    return htmlspecialchars($result);
});

$app->run();

?>
