drop table if exists ipinfos;
create table ipinfos(
  id int not null primary key auto_increment,
  ip_address varchar(50) unique,
  city varchar(50),
  country varchar(50),
  created_at datetime,
  updated_at datetime
);
